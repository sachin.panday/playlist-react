This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:
### `npm install`
### `npm install pm2 -g`
### `pm2 start npm -- start OR npm start`
get all pm2 process list
### `pm2 list` 
stop pm2 service
### `pm2 stop <processid from list>` 

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

