import React, { Component } from 'react';
import "./leftMenu.css";
import { Link } from 'react-router-dom';
import { history } from '../redux-container/store';
import { getPlayList, getPlayListSongsById } from '../redux-container/playList/action'

import { connect } from 'react-redux';



class LeftMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activekey: 0,
            submenu:false
        }

    }
    onClickActiveLinkSet = (event, id) => {
        event.preventDefault()

        sessionStorage.setItem('aplid', id)
        this.props.getPlayListSongsById(id)
        history.push(`/playlist/${id}`)
    }
    componentDidMount = async () => {
        await this.props.GetPlayList();
    }
showHide = ()=>{
    this.setState({submenu:!this.state.submenu})
}


    render() {

        const { playList } = this.props;


        return <div className="nav-side-menu">
            <i className="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

            <div className="menu-list">

                <ul id="menu-content" className="menu-content collapse out show">

                    <li  title="Library" >
                        <Link to="/" ><i className="far fa-images"></i> Library</Link>
                    </li>

                    <span> <li data-toggle="collapse" data-target="#sidebar" className="collapsed" >
                        <Link to="/playlist" title=" Play List">  <i className="fas fa-user-alt" ></i>Play List </Link>
                        <span className="arrow" onClick={this.showHide}></span>
                    </li>
                        <ul className={`sub-menu collapse ${this.state.submenu ? 'show':''}`} id="sidebar">
                            {playList && playList.map((plist, index) => {
                                return <li key={index} className='active' title={plist.name}><Link to="/playlist" onClick={(event) => this.onClickActiveLinkSet(event, plist.id)} >{plist.name}</Link></li>
                            })}
                        </ul>
                    </span>



                </ul>
            </div>
        </div>
    }


}

const mapStateToProps = (state) => {
    return {
        playList: state.playlistData.playList,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        GetPlayList: () => dispatch(getPlayList()),
        getPlayListSongsById: (id) => dispatch(getPlayListSongsById(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenu);