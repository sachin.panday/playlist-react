import React from 'react';
import './songCard.css';

const SongCard = props => {
  return <div className="card-1" key={props.id}>


    <div className="card_image" >
      <img src="https://stl.tech/dev/music.jpg" alt='Loading...' />
      <span className=" btn-1 btn-primary add-song-btn" onClick={(e,id)=>props.onClick(e,props.id)}>+</span>
      <p className="card_title title-black album-title"> Album: {props.album.substring(0, 10)}{
        props.album.length > 10 && <span>...</span>
      }</p>
    </div>
    
   <div className="song-album-title">
    <div className="card_title title-black" >
      <p>{props.title.substring(0, 10)}{
        props.title.length > 10 && <span>...</span>
      }</p>
     
    </div>
    
    </div>
  </div>
}
export default SongCard;
