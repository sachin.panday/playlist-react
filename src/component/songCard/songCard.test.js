import React from 'react';
import { shallow } from 'enzyme';
import SongCard from './songCard';

describe('<SongCard />', () => {
  test('renders', () => {
    const wrapper = shallow(<SongCard />);
    expect(wrapper).toMatchSnapshot();
  });
});
