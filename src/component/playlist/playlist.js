import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { history } from '../../redux-container/store';
import './playlist.css';
import { getPlayList, getPlayListSongsById, deletePlayList } from '../../redux-container/playList/action'



class Playlist extends Component {
  state = { loader: false }
  componentDidMount = async () => {
    this.setState({ loader: true })
    await this.props.GetPlayList();
    this.setState({ loader: false })
    if(this.props.plError){
      NotificationManager.error('PlayList', this.props.plError,5000);
    }
   
  }
  onClickPlay = (event, id) => {
    event.preventDefault()

    sessionStorage.setItem('aplid', id)
    this.props.getPlayListSongsById(id)
    history.push(`/playlist/${id}`)
}

  onClickDelete = async(id) => {
    this.setState({loader:true})
    await this.props.deletePlayList(id);
    this.setState({loader:false})
  }
  render() {
    const { playList } = this.props;
    if (this.state.loader) {
      return <div className='loader'></div>
    }
    return <div >
      <div className='container'>
        <div className="row p-4 playlist-page">
          {playList && playList.length>0 ? playList.map((plist, index) => {
            return <div className="col-sm-3 p-2" key={index}>
              <div className="card p-2">
                <div className="card-body">
                <span onClick={() => this.onClickDelete(plist.id)} className="btn btn-primary  btn-delete-playlist"><i className="fa fa-trash" aria-hidden="true"></i></span>
                  <h5 className="card-title" onClick={(event) => this.onClickPlay(event, plist.id)}>{plist.name}</h5>
                  {/* <p className="card-text">Discription</p> */}
                  {/* <button onClick={() => this.onClickPlay(plist.id)} className="btn btn-primary">PlayAll</button> */}
                </div>
              </div>
            </div>
          }) :"No Data Found"}
        </div>
        <NotificationContainer />
        <br />
      </div>
    </div>
  }
}
const mapStateToProps = (state) => {
  console.log(state.playlistData)
  return {
    playList: state.playlistData.playList,
    songlist: state.playlistData.songList,
    plError:state.playlistData.error

  }
}
const mapDispatchToProps = dispatch => {
  return {
    GetPlayList: () => dispatch(getPlayList()),
    getPlayListSongsById: (id) => dispatch(getPlayListSongsById(id)),
    deletePlayList: (id) => dispatch(deletePlayList(id))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Playlist);
