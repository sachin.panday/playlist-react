import React from 'react';
import { shallow } from 'enzyme';
import Playlist from './playlist';

describe('<Playlist />', () => {
  test('renders', () => {
    const wrapper = shallow(<Playlist />);
    expect(wrapper).toMatchSnapshot();
  });
});
