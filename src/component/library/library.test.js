import React from 'react';
import { shallow } from 'enzyme';
import Library from './library';

describe('<Library />', () => {
  test('renders', () => {
    const wrapper = shallow(<Library />);
    expect(wrapper).toMatchSnapshot();
  });
});
