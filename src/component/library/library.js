import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import Modal from 'react-bootstrap/Modal';
import { getLibraryList, getLibraryListBySearch } from '../../redux-container/Library/action';
import { getPlayList, addSongtoPlayList, addNewPlayList } from '../../redux-container/playList/action'

import './library.css';

import ListDisplay from '../listDisplay'

class Library extends Component {
  constructor(props) {
    super(props);
    this.state = {
      plname: '',
     
      isOpen: false,
      open: false
    }
  }
  componentDidMount = async () => {

    this.setState({ loader: true });
    await this.props.GetLibraryList();
    this.setState({ loader: false });
    if(this.props.libraryError){
      NotificationManager.error('Error', this.props.libraryError,5000);
    }
   


  }
  onclickbutton = async(e, id) => {
    e.preventDefault()
    await this.props.GetPlayList()
    console.log("onclick button", id)
    this.setState({ isOpen: true, songid: id })
  }
  close = () => {
    this.setState({ isOpen: false, open: false, plname: '', songname: '', })
  }
  closeSecond = () => {
    this.setState({ open: false, plname: '', })

  }
  addSongtoPlayList = async (item) => {
    console.log(item, item.id)
    this.setState({ loader: true });

    let arr = [];
    arr = await item.albums.map(temp => temp.id)
    console.log(arr)
    arr.push(this.state.songid)
    console.log(arr)

    await this.props.addSongtoPlayList(item.id, arr);
    this.close()
    this.setState({ loader: false });
    if(this.props.plError){
      await NotificationManager.error('', this.props.plErroe,2000);

    }else{
      await NotificationManager.success('', 'Song Add Successfully Playlist ',2000);

    }


  }
  onOpenSecondMaodal = () => {
    this.setState({ open: true })
  }
  onCreatePlaylist = async () => {
    this.setState({ loader: true });

    let temp = []
    temp.push(this.state.songid)
    console.log(this.state.plname, temp)
    await this.props.addNewPlayList(this.state.plname, temp)
    this.close()
    this.setState({ loader: false });
    if(this.props.plError){
      await NotificationManager.error('', this.props.plErroe,2000);

    }else{
    await NotificationManager.success('', 'Playlist Create Successfully',2000);

    }
  }
  onChangeInput = (event) => {
    const { id, value } = event.target
    this.setState({ [id]: value })

   
  }
  render() {
    const { plname } = this.state
    console.log(this.state)
    let PlayListModal, playlistCreateModal;
    if (this.state.isOpen) {
      PlayListModal = <div>
        <Modal size="lg" className="modal-container" show={this.state.isOpen} onHide={() => this.close()}>
          <Modal.Header closeButton>
            <Modal.Title >Playlist to add to  </Modal.Title>
          </Modal.Header>
          <Modal.Body  >
            <div className="card" style={{ "width": "100%" }}>
              <div className="card-body">
              <ul className="list-group list-group-flush">
                {this.props.playList && this.props.playList.map((item, index) => {
                  return <li key={index} className="list-group-item" onClick={() => this.addSongtoPlayList(item)}><b>Title :</b> {item.name}<br /> </li>

                })}
              </ul>
              <p className=" btn btn-primary" onClick={this.onOpenSecondMaodal}>Create New PlayList</p>
              </div>
              </div>
          
          </Modal.Body>
        </Modal>
        <br />
      </div>

    }
    if (this.state.open) {
      playlistCreateModal = <div className="create-playlist-modal">
        <Modal className="modal-container newPlaylistModal" show={this.state.open} onHide={() => this.closeSecond()}>
          <Modal.Header closeButton>
            <Modal.Title > <p>Create New PlayList</p> </Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ padding: '0' }} >
            <div className="card " style={{ "width": "100%", margin: 'auto ', }}>
              {/* <h4>List of songs</h4> */}

              <div className="card-body " >

                <div >
                <label>
                  Playlist Name
                  <input type='text'  className='form-control' style={{ marginRight: '1rem' }} onChange={this.onChangeInput} id='plname' value={plname} placeholder="My Playlist" />
                  <button className='btn btn-primary btn-sm createNewP' onClick={this.onCreatePlaylist} >Create</button>
                  </label>
                  
                </div>
              </div>

            </div>
          </Modal.Body>
        </Modal>
      </div>
    }

    const { librarydata } = this.props
    if (this.state.loader) {
      return <div className="loader">

      </div>
    }
    return <Fragment>
      {librarydata && librarydata.length>0 ? <div className='container fixHeightDiv'>
         <ListDisplay data={librarydata} onClick={this.onclickbutton} />
      </div>:<h2>No Record Found</h2>}
      {PlayListModal}
      {playlistCreateModal}
      <NotificationContainer />
      <br />


    </Fragment>
  }
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    librarydata: state.library.listOfLibraryList,
    playList: state.playlistData.playList,
    libraryError:state.library.error,
    plError:state.playlistData.error

  }
}
const mapDispatchToProps = dispatch => {
  return {
    GetLibraryList: () => dispatch(getLibraryList()),
    getLibraryListBySearch: (name) => dispatch(getLibraryListBySearch(name)),
    GetPlayList: () => dispatch(getPlayList()),
    addSongtoPlayList: (plId, songList) => dispatch(addSongtoPlayList(plId, songList)),
    addNewPlayList: (name, songList) => dispatch(addNewPlayList(name, songList))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Library);
