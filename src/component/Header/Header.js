import React, { Component } from 'react';
import { connect } from 'react-redux';
import { history } from '../../redux-container/store';

import { getLibraryList, getLibraryListBySearch, getLibraryListByDuration } from '../../redux-container/Library/action';
import './Header.css';
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      songname: '',
      durationslot: '',
      duration: [{ "name": "Less Then 200", time: 200, field: "durationless" },
      { "name": "Greater Then 200", time: 200, field: "durationgreater" },
      { "name": "Less Then 400", time: 400, field: "durationless" },
      { "name": "Greater Then 400", time: 400, field: "durationgreater" },
      { "name": "Less Then 600", time: 600, field: "durationless" },
      { "name": "Greater Then 600", time: 600, field: "durationgreater" }
      ]
    }
  }

  onChangeInput = async (event) => {
    event.preventDefault()
    // this.setState({ songname: '',durationslot:""})
    
    const { duration } = this.state
    const { id, value } = event.target
    console.log(value)

    this.setState({ [id]: value })
    if (id === 'songname' && value.length > 2) {
      await this.props.getLibraryListBySearch(value)
      this.setState({durationslot:""})
      history.push('/')

    }
    if (id === 'songname' && value.length < 1) {
      this.setState({durationslot:""})
      history.push('/')
      await this.props.GetLibraryList()
     
    }
    if (id === 'durationslot' && value !== "none") {
      this.setState({songname:""})
      let result =  duration.filter(item => item.name === value);
      await this.props.getLibraryListByDuration(result[0].time, result[0].field)
     await history.push('/')
      console.log(result)
    }
    if (id === 'durationslot' && value === "none") {
      this.setState({songname:""})
      await this.props.GetLibraryList();
      history.push('/')
    }
  }
  onChangeInput1= event=>{
    console.log(event.target.value)
    this.setState({songname:event.target.value})
  }

  render() {
    const { songname } = this.state
    return <div className=" playlist-row">
      <div className='searchbar-container'>
        <div className='container'>
          <img src="https://stl.tech/dev/mmmm.png" alt="..." />
          <div className="right-header">
          <span className="example"   >
            <input type="text" placeholder="Search.."  onChange={this.onChangeInput}   id="songname" value={songname} />
           </span>
           <div className="playlist-btn-container form-inline">
            <label>
            <span>Duration</span>
          <select id="durationslot"
                onChange={this.onChangeInput}
                value={this.state.durationslot}
                className="form-control" >
                <option value="none">Choose...</option>
                {this.state.duration.map((item, index) => {
                  return <option key={index} value={item.name}>{item.name}</option>
                })}

              </select>
            </label>
          </div>
          
          
           
          
          
          </div>
         
        </div>
      </div>
    </div>

  }
}

const mapStateToProps = (state) => {
  console.log(state.playlistData)
  return {
    playList: state.playlistData.playList,
    songlist: state.playlistData.songList,

  }
}
const mapDispatchToProps = dispatch => {
  return {
    GetLibraryList: () => dispatch(getLibraryList()),
    getLibraryListBySearch: (name) => dispatch(getLibraryListBySearch(name)),
    getLibraryListByDuration: (time, field) => dispatch(getLibraryListByDuration(time, field))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);