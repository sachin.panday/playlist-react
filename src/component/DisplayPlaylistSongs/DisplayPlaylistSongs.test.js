import React from 'react';
import { shallow } from 'enzyme';
import DisplayPlaylistSongs from './DisplayPlaylistSongs';

describe('<DisplayPlaylistSongs />', () => {
  test('renders', () => {
    const wrapper = shallow(<DisplayPlaylistSongs />);
    expect(wrapper).toMatchSnapshot();
  });
});
