import React, { Component } from 'react';
import { getPlayList, getPlayListSongsById } from '../../redux-container/playList/action'
import { connect } from 'react-redux';


class DisplayPlaylistSongs extends Component {

  constructor(props){
    super(props)
    this.state={}
  }
  componentDidMount=async()=>{
    await this.props.getPlayListSongsById(sessionStorage.getItem("aplid"))
  }
  render() {
    const {  songlist } = this.props;

    if(this.props.loader){
      return <div className="loader"></div>
    }
    return <div>
      <div className="container-fluid playlist-container-fluid">
       <div className="row">
        <div className="col-sm-12 ">
          <div className="card p-4">
            {/* <h4>List of songs</h4> */}
            {/* <img src="..." className="card-img-top" alt="..." /> */}
            <div className="card-body">
              <h5 className="card-title">{songlist && songlist.name}</h5>
              {/* <p className="card-text">Discription About Playlist</p> */}
            </div>
            <div class="table-responsive">
            <table className="table table-dark table-hover">
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Album</th>
              <th>Artist</th>
              <th>Duration</th>
            </tr>
              {songlist && songlist.albums.map((item, index) => {
                return <tr key={index} className="">
                <td>{item.id} </td>
                <td>{item.title}</td>
                <td>{item.album} </td>
                <td>{item.artist}</td>
                <td>{item.duration}</td>
                </tr>
              })}
            </table>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>;
  }
}

const mapStateToProps = (state) => {
  return {
      songlist: state.playlistData.songList,
      loader:state.playlistData.isLoader,
  }
}
const mapDispatchToProps = dispatch => {
  return {
      GetPlayList: () => dispatch(getPlayList()),
      getPlayListSongsById: (id) => dispatch(getPlayListSongsById(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DisplayPlaylistSongs);