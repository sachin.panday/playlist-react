import React from 'react';
import SongCard from '../songCard';

import './listDisplay.css'


const ListDisplay = props => {
  return <div className="cards-list ">
    {props.data.map((item, index) => {
      return <SongCard
      key={index}
        title={item.title}
        album={item.album}
        id={item.id}
        onClick={props.onClick}
      />
    })}

  </div>
}
export default ListDisplay;
