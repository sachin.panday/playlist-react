import React from 'react';
import { shallow } from 'enzyme';
import ListDisplay from './listDisplay';

describe('<ListDisplay />', () => {
  test('renders', () => {
    const wrapper = shallow(<ListDisplay />);
    expect(wrapper).toMatchSnapshot();
  });
});
