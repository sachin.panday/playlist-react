import React, { Component, Fragment } from 'react';
import './App.css';
import 'react-notifications/lib/notifications.css';
import { Route, Switch } from 'react-router'
import { connect } from 'react-redux';
import LeftMenu from './leftMenu'
import Library from './component/library'
import Playlist from './component/playlist';
import DisplayPlaylistSongs from './component/DisplayPlaylistSongs';
import Header from './component/Header'


class App extends Component {
  render() {
    return (
      <Fragment>
         <div className='maincontainer'>
           <Header />
        <div className='leftcontainer'>
         <LeftMenu isMenuOpen='true'></LeftMenu>
          <div className="contentcontainerOpen">
        <Switch>
          <Route exact path="/playlist">
            <Playlist />
          </Route>   
          <Route exact path="/">
            <Library />
          </Route>
          <Route exact path="/playlist/:id">
            <DisplayPlaylistSongs />
          </Route>
        </Switch>
        </div>
        </div>
        </div>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {


  }
}
const mapDispatchToProps = dispatch => {
  return {
    // getUserData:() => dispatch(getUserData()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
