import baseURL from '../../helper/baseURL';

import { toastr } from 'react-redux-toastr'
import { GET_PLAYLIST_REQUEST, GET_SONGS_LIST_BY_PLAYLIST_ID, DELETE_PLAYLIST, ADD_SONG_TO_PLAYLIST, ADD_NEW_PLAYLIST, REQUEST_FAILURE, FETCH_REQUEST } from './type'


export const getPlayList = () => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const query = `{fetchPlayList{
        id
        name
        albums{
            
            id
            
          }
          }
      }`
    await baseURL.post('', {
        query,
    })

        .then(response => {
            if (response) {
                dispatch({ type: GET_PLAYLIST_REQUEST, payload: response.data.data.fetchPlayList });
            }
        })
        .catch(error => {
            // toastr.error(error.response.data['message'])
            dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
        })
}

export const getPlayListSongsById = (id) => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const query = `{getPlayList(id:${id}){
        id
        name
        albums{
          album
          title
          id
          duration
          artist
        }
      }
      }`
    await baseURL.post('', {
        query,
    })

        .then(response => {
            if (response) {
                dispatch({ type: GET_SONGS_LIST_BY_PLAYLIST_ID, payload: response.data.data.getPlayList });
            }
        })
        .catch(error => {
            toastr.error(error)
            dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
        })
}

export const addSongtoPlayList = (plId, songList) => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })

    const mutation = `mutation{
        playListUpdateSongs(req:{id:${parseFloat(plId)},songs:[${songList}]}){id  
            name 
            albums{
          id
        }}
          }`
    await baseURL.post('', {
        query: mutation,
    })

        .then(response => {
            if (response) {

                dispatch({ type: ADD_SONG_TO_PLAYLIST, payload: response.data });
            }
        })
        .catch(error => {
            // toastr.error(error['message'])
            dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
        })
}
export const addNewPlayList = (name, songList) => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })

    const mutation = `mutation{
                playListAdd(req:{name: "${name}", songs:[${songList}]}) {
                  id
                  name
                  albums {
                    id
                    title
                    album
                    duration
                  }
                }
              }`
    await baseURL.post('', {
        query: mutation,
    })
    await dispatch(getPlayList())
        .then(response => {
            if (response) {
                dispatch({ type: ADD_NEW_PLAYLIST, payload: response.data.data.getPlayList });
            }
        })
        .catch(error => {
            // toastr.error(error['message'])
            dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
        })
}
export const deletePlayList = (id) => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const mutation = `mutation{
        playListRemove(id:${parseFloat(id)}) {
                  id
                  name
                  
                }
              }`
    await baseURL.post('', {
        query: mutation,
    })
    await dispatch(getPlayList())

        .then(response => {
            if (response) {
                dispatch({ type: DELETE_PLAYLIST, payload: response.data });
            }
        })
        .catch(error => {
            // toastr.error(error['message'])
            dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
        })
}
