import { GET_PLAYLIST_REQUEST,GET_SONGS_LIST_BY_PLAYLIST_ID, ADD_SONG_TO_PLAYLIST,DELETE_PLAYLIST, REQUEST_FAILURE, FETCH_REQUEST } from './type';



const playlistReducer = (state = { isLoader: true }, action) => {
    switch (action.type) {
        case FETCH_REQUEST:
            return {
                ...state,
                isLoader: true,
            }
      
        case GET_PLAYLIST_REQUEST:
            return {
                ...state,
                playList: action.payload,
                isLoader: false,
            }
        case GET_SONGS_LIST_BY_PLAYLIST_ID:
            return{
                ...state,
                songList:action.payload,
                isLoader: false,
                
            }
        case ADD_SONG_TO_PLAYLIST:
            return{
                ...state,
                [action.payload]:action.payload,
                isLoader: false,
            }
            case DELETE_PLAYLIST:
                return{
                    ...state,
                    [action.payload]:action.payload,
                    isLoader: false,
                }
        case REQUEST_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoader: false,

            }
        default: return state;
    }

}
export default playlistReducer;