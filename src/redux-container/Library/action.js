import baseURL  from '../../helper/baseURL';

import { GET_LIBRARY_LIST_REQUEST, REQUEST_FAILURE, FETCH_REQUEST } from './type'


export const getLibraryList =  ()  => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const query = `{
        fetchLibrary {
             id
            title
            album
            duration
            artist
        }
      }`
   await baseURL.post('', {
        query,
        
      })
    
    .then(response => {
        // console.log(response)
        if (response) {
            
            dispatch({ type: GET_LIBRARY_LIST_REQUEST, payload: response.data.data.fetchLibrary });
        }
    })
    .catch(error => {
        dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
    })
}
export const getLibraryListBySearch =  (name)  => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const query = `{
        getLibraryBySearchString(searchString:"${name}"){
             id
            title
            album
            duration
            artist
        }
      }`
   await baseURL.post('', {
        query,
        
      })
    
    .then(response => {
        if (response) {
            dispatch({ type: GET_LIBRARY_LIST_REQUEST, payload: response.data.data.getLibraryBySearchString  });
        }
    })
    .catch(error => {
       
        dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
    })
}

export const getLibraryListByDuration =  (time,field)  => async (dispatch) => {
    dispatch({ type: FETCH_REQUEST })
    const query = `{
        getLibraryByTime(key:"${field}",time:${time}){
             id
            title
            album
            duration
            artist
        }
      }`
   await baseURL.post('', {
        query,
        
      })
    
    .then(response => {
        if (response) {
            dispatch({ type: GET_LIBRARY_LIST_REQUEST, payload: response.data.data.getLibraryByTime  });
        }
    })
    .catch(error => {
        // toastr.error(error['message'])
        dispatch({ type: REQUEST_FAILURE, payload: error['message'] });
    })
}
