import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import libraryReducer from './Library/reducer';
import playlistReducer from './playList/reducer'

const createRootReducer = (history) => combineReducers({
    library:libraryReducer,
    playlistData:playlistReducer,

    router: connectRouter(history),
  
})
export default createRootReducer
